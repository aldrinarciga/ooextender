package main.utils;

import main.controllers.MainController;
import main.models.AWSInfo;
import main.models.LogType;


import static main.utils.FileUtils.*;

/**
 * Created by aldrinarciga on 8/10/2017.
 */
public class Extender {
    public static void initiateExtension(MainController mainController) {
        String projectRootFolder = mainController.txtProjLoc.getText() + "\\";
        String assetsRootFolder = mainController.txtAssetsLoc.getText() + "\\";
        String flavorName = mainController.txtFlavorName.getText();
        String templateFolder = projectRootFolder + "app\\src\\template\\";
        String newFolder = projectRootFolder + "app\\src\\" + flavorName + "\\";

        //Copy the template folder first
        mainController.addToLog("Creating flavor " + flavorName + "...", LogType.NORMAL);
        copyFolder(templateFolder, newFolder);
        mainController.addToLog(flavorName + " is created", LogType.NORMAL);

        //Copy asstes
        mainController.addToLog("Copying assets...", LogType.NORMAL);
        copyImages(mainController, assetsRootFolder, newFolder);
        mainController.addToLog("Finished copying assets", LogType.NORMAL);

        //Update string values
        mainController.addToLog("Updating string values...", LogType.NORMAL);
        updateStrings(mainController, assetsRootFolder, newFolder);
        mainController.addToLog("Finished updating values", LogType.NORMAL);

        //Update gradle
        mainController.addToLog("Updating gradle...", LogType.NORMAL);
        updateGradle(mainController, projectRootFolder);
        mainController.addToLog("Finished updating gradle", LogType.NORMAL);
    }

    private static void updateGradle(MainController mainController, String projectRootFolder) {
        String gradleFolder = projectRootFolder + "app\\build.gradle";
        try {
            String contents = getFileContents(gradleFolder);
            String gradleTemplate = "{flavor_name} {\n" +
                    "            applicationId 'store.dpos.com.{flavor_name}'\n" +
                    "            manifestPlaceholders = [accountId: {account_id}]\n" +
                    "        }\n" +
                    "        //{new_build}";
            String newContent = gradleTemplate.replace("{flavor_name}", mainController.txtFlavorName.getText());
            newContent = newContent.replace("{account_id}", mainController.txtAccountId.getText());
            contents = contents.replace("//{new_build}", newContent);
            rewriteFileContents(gradleFolder, contents);
        } catch (Exception ex) {
            mainController.addToLog("Some error: ", LogType.ERROR);
            mainController.addToLog(ex.getLocalizedMessage(), LogType.ERROR);
            mainController.addToLog("Suggested Actions:", LogType.ACTION);
            mainController.addToLog("(1) Manually update gradle file", LogType.ACTION);
        }
    }

    private static void updateStrings(MainController mainController, String assetsRootFolder, String newFolder) {
        String stringsDir = newFolder + "res\\values\\strings.xml";
        String awsDir = assetsRootFolder + "AWSConfiguration.java";
        try {
            String contents = getFileContents(stringsDir);
            contents = contents.replace("{{app_name}}", mainController.txtStoreName.getText());
            contents = contents.replace("{{facebook_app_id}}", mainController.txtFacebookAppId.getText());
            contents = contents.replace("{{google_maps_key}}", mainController.txtGoogleAPIKey.getText());

            AWSInfo info = AWSInfo.infoFromController(mainController);
            contents = contents.replace("{{user_agent}}", info.getUserAgent());
            contents = contents.replace("{{cognito_id}}", info.getCognitoId());
            contents = contents.replace("{{gcm_sender_id}}", info.getGcmSenderId());
            contents = contents.replace("{{platform_arn}}", info.getPlatformArn());
            contents = contents.replace("{{topic_arn}}", info.getTopicArn());
            contents = contents.replace("{{cognito_region}}", info.getCognitoRegion());
            contents = contents.replace("{{sns_region}}", info.getSnsRegion());

            rewriteFileContents(stringsDir, contents);

        } catch (Exception ex) {
            mainController.addToLog("Some error: ", LogType.ERROR);
            mainController.addToLog(ex.getLocalizedMessage(), LogType.ERROR);
            mainController.addToLog("Suggested Actions:", LogType.ACTION);
            mainController.addToLog("(1) Manually update strings file", LogType.ACTION);
        }

    }

    private static void copyImages(MainController mainController, String assetsRootFolder, String newFolder) {
        String[] otherDrawables = {
                "drawable-hdpi",
                "drawable-mdpi",
                "drawable-xhdpi"
        };

        String[] mipmaps = {
                "mipmap-hdpi",
                "mipmap-mdpi",
                "mipmap-xhdpi",
                "mipmap-xxhdpi",
                "mipmap-xxxhdpi"
        };
        String resDrawable = newFolder + "res\\drawable\\";
        try {
            copyFile(assetsRootFolder + "google-services.json", newFolder + "google-services.json");

            copyFile(assetsRootFolder + "launch.png", resDrawable + "launch.png");
            copyFile(assetsRootFolder + "logo.png", resDrawable + "login_logo1.png");
            copyFile(assetsRootFolder + "logo.png", resDrawable + "logo.png");
            copyFile(assetsRootFolder + "main_background.png", resDrawable + "main_background.png");

            for(String drw : otherDrawables) {
                String other = newFolder + "res\\" + drw + "\\";
                copyFile(assetsRootFolder + "main_background.png", other + "main_background.png");
            }

            for(String mp : mipmaps) {
                String other = newFolder + "res\\" + mp + "\\";
                copyFile(assetsRootFolder + "ic_launcher.png", other + "ic_launcher.png");
            }
        } catch (Exception ex) {
            mainController.addToLog("Some error: ", LogType.ERROR);
            mainController.addToLog(ex.getLocalizedMessage(), LogType.ERROR);
            mainController.addToLog("Suggested Actions:", LogType.ACTION);
            mainController.addToLog("(1) Manually copy images into right directories", LogType.ACTION);
        }
    }

}
