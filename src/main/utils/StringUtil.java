package main.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by aldrinarciga on 8/10/2017.
 */
public class StringUtil {
    public static String getStringBetween(String str, String from, String to) {
        String regexString = Pattern.quote(from) + "(.*?)" + Pattern.quote(to);
        Pattern pattern = Pattern.compile(regexString);
        Matcher matcher = pattern.matcher(str);

        if (matcher.find()) {
            return matcher.group(1);
        }
        return "";
    }
}
