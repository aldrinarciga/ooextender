package main.utils;

import main.controllers.MainController;
import main.models.LogType;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by aldrinarciga on 8/10/2017.
 */
public class Validator {
    public static boolean isAllFieldsValid(MainController mainController) {
        boolean isValid = true;
        boolean isAWSErr = false;

        if(mainController.txtStoreName.getText().isEmpty()) {
            isValid = false;
            mainController.addToLog("Store name is invalid", LogType.ERROR);
        } else if(mainController.txtFlavorName.getText().isEmpty()) {
            isValid = false;
            mainController.addToLog("Flavor name is invalid", LogType.ERROR);
        } else if(mainController.txtFacebookAppId.getText().isEmpty()) {
            isValid = false;
            mainController.addToLog("Facebook App ID is invalid", LogType.ERROR);
        } else if(mainController.txtAccountId.getText().isEmpty()) {
            isValid = false;
            mainController.addToLog("Account ID is invalid", LogType.ERROR);
        } else if(mainController.txtGoogleAPIKey.getText().isEmpty()) {
            isValid = false;
            mainController.addToLog("Google API Key is invalid", LogType.ERROR);
        } else if(mainController.txtSenderKey.getText().isEmpty()) {
            isValid = false;
            mainController.addToLog("Sender Key is invalid", LogType.ERROR);
        } else if(mainController.txtProjLoc.getText().isEmpty()) {
            isValid = false;
            mainController.addToLog("Please select project location", LogType.ERROR);
        } else if(mainController.txtAssetsLoc.getText().isEmpty()) {
            isValid = false;
            mainController.addToLog("Please select assets location", LogType.ERROR);
        } else if(mainController.txtMobileHubId.getText().isEmpty()) {
            isValid = false;
            isAWSErr = true;
            mainController.addToLog("Please complete AWS info", LogType.ERROR);
        } else if(mainController.txtRegion.getText().isEmpty()) {
            isValid = false;
            isAWSErr = true;
            mainController.addToLog("Please complete AWS info", LogType.ERROR);
        } else if(mainController.txtPoolId.getText().isEmpty()) {
            isValid = false;
            isAWSErr = true;
            mainController.addToLog("Please complete AWS info", LogType.ERROR);
        } else if(mainController.txtPlatformArn.getText().isEmpty()) {
            isValid = false;
            isAWSErr = true;
            mainController.addToLog("Please complete AWS info", LogType.ERROR);
        } else if(mainController.txtTopicArn.getText().isEmpty()) {
            isValid = false;
            isAWSErr = true;
            mainController.addToLog("Please complete AWS info", LogType.ERROR);
        }

        if(isValid) {
            isValid = isProjectFolderValid(mainController.txtProjLoc.getText());
            if(!isValid) {
                mainController.addToLog("Project location is invalid", LogType.ERROR);
                mainController.addToLog("Suggested Actions:", LogType.ACTION);
                mainController.addToLog("(1) Check if you are in the right directory", LogType.ACTION);
                mainController.addToLog("(2) Make sure you are checked out into extensions branch", LogType.ACTION);
            }
        }

        if(isValid) {
            isValid = isAssetsFolderValid(mainController);
        }

        if(isAWSErr) {
            mainController.addToLog("If you are unsure where to get their values, please check the OO Extension Guide", LogType.ACTION);
        }

        return isValid;
    }

    private static boolean isProjectFolderValid(String rootFolder) {
        String templateFolder = rootFolder + "\\app\\src\\template";
        return (new File(templateFolder).exists());
    }

    private static boolean isAssetsFolderValid(MainController mainController) {
        boolean isValid = true;
        String rootFolder = mainController.txtAssetsLoc.getText();
        ArrayList<String> missingFiles = new ArrayList<>();
        String[] requiredFiles = {
                "launch.png",
                "logo.png",
                "main_background.png",
                "ic_launcher.png",
                "google-services.json"
        };


        for(String requiredFile : requiredFiles) {
            if(!(new File(rootFolder + "\\" + requiredFile)).exists()) {
                missingFiles.add(requiredFile);
            }
        }

        isValid = missingFiles.isEmpty();
        if(!isValid) {
            mainController.addToLog("Some files are missing in the assets folder", LogType.ERROR);
            mainController.addToLog("Missing files:", LogType.ACTION);
            int num = 0;
            for(String str :missingFiles) {
                num++;
                mainController.addToLog("(" + num + ") " + str, LogType.ACTION);
            }
        }

        return isValid;
    }
}
