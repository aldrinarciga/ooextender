package main.models;

/**
 * Created by aldrinarciga on 8/10/2017.
 */
public enum LogType {
    NORMAL("green"), ERROR("red"), DEFAULT("blue"), START_END("white"), ACTION("orange");

    private String value;

    LogType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
