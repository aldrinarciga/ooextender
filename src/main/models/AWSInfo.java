package main.models;

import main.controllers.MainController;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static main.utils.StringUtil.getStringBetween;

/**
 * Created by aldrinarciga on 8/10/2017.
 */
public class AWSInfo {
    private String userAgent;
    private String cognitoId;
    private String gcmSenderId;
    private String platformArn;
    private String topicArn;
    private String cognitoRegion;
    private String snsRegion;

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getCognitoId() {
        return cognitoId;
    }

    public void setCognitoId(String cognitoId) {
        this.cognitoId = cognitoId;
    }

    public String getGcmSenderId() {
        return gcmSenderId;
    }

    public void setGcmSenderId(String gcmSenderId) {
        this.gcmSenderId = gcmSenderId;
    }

    public String getPlatformArn() {
        return platformArn;
    }

    public void setPlatformArn(String platformArn) {
        this.platformArn = platformArn;
    }

    public String getTopicArn() {
        return topicArn;
    }

    public void setTopicArn(String topicArn) {
        this.topicArn = topicArn;
    }

    public String getCognitoRegion() {
        return cognitoRegion;
    }

    public void setCognitoRegion(String cognitoRegion) {
        this.cognitoRegion = cognitoRegion;
    }

    public String getSnsRegion() {
        return snsRegion;
    }

    public void setSnsRegion(String snsRegion) {
        this.snsRegion = snsRegion;
    }

    public static AWSInfo infoFromString(String content) {
        AWSInfo awsInfo = new AWSInfo();
        awsInfo.setUserAgent(getStringBetween(content, "public static final String AWS_MOBILEHUB_USER_AGENT =\n" +
                "        \"", "\";\n" +
                "    // AMAZON COGNITO"));
        awsInfo.setCognitoRegion(getStringBetween(content, "AMAZON_COGNITO_REGION =\n" +
                "      Regions.fromName(\"", "\");\n" +
                "    public static final String  AMAZON_COGNITO_IDENTITY_POOL_ID"));
        awsInfo.setCognitoId(getStringBetween(content, "AMAZON_COGNITO_IDENTITY_POOL_ID =\n" +
                "        \"", "\";\n" +
                "    // GOOGLE CLOUD MESSAGING SENDER ID"));
        awsInfo.setGcmSenderId(getStringBetween(content, "GOOGLE_CLOUD_MESSAGING_SENDER_ID =\n" +
                "        \"", "\";\n" +
                "    // SNS PLATFORM APPLICATION ARN"));
        awsInfo.setPlatformArn(getStringBetween(content, "AMAZON_SNS_PLATFORM_APPLICATION_ARN =\n" +
                "        \"", "\";\n" +
                "    public static final Regions AMAZON_SNS_REGION"));
        awsInfo.setSnsRegion(getStringBetween(content, "AMAZON_SNS_REGION =\n" +
                "         Regions.fromName(\"", "\");\n" +
                "    // SNS DEFAULT TOPIC ARN"));
        awsInfo.setTopicArn(getStringBetween(content, "AMAZON_SNS_DEFAULT_TOPIC_ARN =\n" +
                "        \"", "\";\n" +
                "    // SNS PLATFORM TOPIC ARNS"));
        return awsInfo;
    }

    public static AWSInfo infoFromController(MainController controller) {
        AWSInfo awsInfo = new AWSInfo();
        String userAgent = "MobileHub "+ controller.txtMobileHubId.getText() +" aws-my-sample-app-android-v0.18";
        String region = controller.txtRegion.getText();
        awsInfo.setUserAgent(userAgent);
        awsInfo.setCognitoRegion(region);
        awsInfo.setCognitoId(region + ":" + controller.txtPoolId.getText());
        awsInfo.setGcmSenderId(controller.txtSenderKey.getText());
        awsInfo.setPlatformArn(controller.txtPlatformArn.getText());
        awsInfo.setSnsRegion(region);
        awsInfo.setTopicArn(controller.txtTopicArn.getText());
        return awsInfo;
    }

}
