package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.controllers.MainController;

public class Main extends Application {

    Stage primaryStage;

    @Override
    public void start(Stage ps) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("fxmls/main_controller.fxml"));
        Parent root = loader.load();
        Initializable controller = loader.getController();
        primaryStage = ps;
        if(controller instanceof MainController) {
            ((MainController) controller).setStage(primaryStage);
        }
        primaryStage.setTitle("OO App Extender 1.1");
        primaryStage.setScene(new Scene(root, 330, 640));
        primaryStage.setResizable(false);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
