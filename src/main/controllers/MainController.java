package main.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import main.models.LogType;
import main.utils.Extender;
import main.utils.Validator;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable{
    public TextField txtFlavorName;
    public TextField txtFacebookAppId;
    public TextField txtAccountId;
    public TextField txtGoogleAPIKey;
    public TextField txtProjLoc;
    public TextField txtAssetsLoc;
    public TextFlow txtLog;

    public Stage stage;
    public Button btnExtend;
    public ScrollPane scrollLog;
    public TextField txtStoreName;
    public TextField txtSenderKey;
    public TextField txtMobileHubId;
    public TextField txtRegion;
    public TextField txtPoolId;
    public TextField txtPlatformArn;
    public TextField txtTopicArn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void getProjLoc(ActionEvent actionEvent) {
        chooseLocation(true);
    }

    public void getAssetsLoc(ActionEvent actionEvent) {
        chooseLocation(false);
    }

    public void extendApp(ActionEvent actionEvent) {
        clearLogs();
        addToLog("Processing extension...", LogType.START_END);
        btnExtend.setDisable(true);
        if(Validator.isAllFieldsValid(this)) {
            Extender.initiateExtension(this);
            addToLog("Finished extending the app!", LogType.START_END);
        } else {
            addToLog("Terminated extension, please fix all errors", LogType.START_END);
        }
    }


    private void chooseLocation(boolean isProject) {
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Select directory");
        File dir = chooser.showDialog(stage);
        if(dir != null) {
            if(isProject) {
                txtProjLoc.setText(dir.getAbsolutePath());
            } else {
                txtAssetsLoc.setText(dir.getAbsolutePath());
            }
        }
    }

    public void addToLog(String str, LogType type) {
        Text text = new Text();
        text.setStyle("-fx-fill: "+ type.getValue() +";");
        text.setText(">>\t" + str + "\n");
        txtLog.getChildren().add(text);
        if(type == LogType.ERROR || type == LogType.START_END) {
            btnExtend.setDisable(false);
        }
        scrollLog.setVvalue(1);
    }

    public void clearLogs(){
        txtLog.getChildren().clear();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
